# ChemPerturbDB Data Files and Docker

This repository is to store the large data files and docker image for ChemPerturbDB. The introduction and code of ChemPerturbDB is at [github.com/jtc1246/ChemPerturbDB](https://github.com/jtc1246/ChemPerturbDB).

The usage of docker is in [docker.md](/docker.md)
