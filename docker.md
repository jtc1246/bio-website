# Docker Commands

## 1. First time

### Requirements

Fitst need to install git (and git-lfs) and docker if they haven't been installed.

Following instruction is only based on ubuntu or debian (systems using apt).

```bash
sudo apt update
sudo apt install git git-lfs
sudo apt install docker.io
```

### Steps

```bash
git clone https://gitlab.com/jtc1246/bio-website.git
cd bio-website
```

```bash
unzip docker_v1.tar.zip
sudo docker import docker_v1.tar bio-website:v1
```

Then put your webserver directory to resources dir. (must do this BEFORE run following command)

```bash
sudo docker run -d -p 9020:9020 -p 9021:9021 -p 9022:9022 -v <actual_resources_path>:/root/data bio-website:v1 /bin/bash -c "service ssh start; /bin/sh /root/start.sh; while true; do sleep 1; done"
```

\<actual_resources_path\>: Replace to your actual resources path, please use absolute path, not relative path.

You can also change the port, to change, please change the previous 9020, 9021 etc. Don't change the later one. For example, you can change `-p 9020:9020` to `-p 80:9020`, then others can access through port 80, instead of 9020.

#### Port explanation:
1. 9020: for http.
2. 9021: for ssh. When created, you can `ssh root@127.0.0.1 -p 9021` (or the ip of device) to connect.
3. 9022: currently not used, reserved for https.

When created, ssh 127.0.0.1 -p 9021 (or the ip of device) to connect. (Or use `docker exec -it <container_id> /bin/bash`, use this only if ssh doesn't work, just to do basic setup)

You can also open http://\<ip\>:9020 (or the port you set previously) in the browser to view.

## 2. Basic docker operations

**(1) see container id**

```bash
sudo docker ps -a
```
Container id is shown on the leftest column.

**(2) restart / stop / start a container**

First, get its id from (1).
```bash
sudo docker restart <container_id> # restart
sudo docker stop <container_id> # stop
sudo docker start <container_id> # start
```

**(3) delete a container**

First, get its id from (1).
```bash
sudo docker rm -f <container_id>
```

**(4) see image id**

```bash
sudo docker image ls
```
Image id is shown on the third (from left) column.

**(5) delete an image**

First, get its id from (4).
```bash
sudo docker image rm -f <container_id>
```

**(6) how to change ports or resources directory**

Docker doesn't allow users to do this directly. You can only delete the container, and start another one.

## 3. Things to do when you update python, R, or html

Just restart the container (if all file paths have not changed).

## 4. Environment

1. Required R packages installed, R file can run normally.
2. Python and pip installed.
3. No data files (csv, RDS) inside, no R, python or html file inside.
